package sample;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by johan on 3/11/17.
 */
public class ability {
    public String name;
    public List<String> types = new ArrayList<>();
    public int damage;
    public int healing;
    public int ammo;
    public double range;
    public double radius;   // 0 for Direct Damage
    public double duration;
    public double cooldown; // = Reload Speed for Primary, = Recharge Rate for Ultimate
    public int DPS; // Damage Per Second (over 100s with Reload Speed)
    public int HPS; // Healing Per Second (over 100s with Cooldown)

//    DEPLOYABLE RADIUS/RANGE
    String possibleTypes[] = {"damage", "healing", "movement", "shield", "defensive", "deployable", "cc"}; //(defensive = reduced damage)

    public ability (String newName, String newTypes, int newDamage, int newHealing, int newAmmo, double newRange, double newRadius, double newDuration, double newCooldown){
        name = newName;
        damage = newDamage;
        healing = newHealing;
        ammo = newAmmo;
        range = newRange;
        radius = newRadius;
        duration = newDuration;
        cooldown = newCooldown;

        String arr[] = newTypes.replaceAll(" ", "").split(",");
        for (String i : arr){
            for (String j : possibleTypes){
                if (i.equals(j)){
                    types.add(i);
                }
            }
        }
        if (types.size() == 0){
            types.add("INVALID");
        }
    }
}
