package sample;

import java.util.List;

/**
 * Created by johan on 3/13/17.
 */
public class item {
    public String type; // defensive, utility, healing, attack
    public int tier;
    public int cost[] = new int[3];
    public List<String> affects;    // method for items affecting abilities?

    public item() {
        if (tier < 3)
            tier = 3;
        if (tier > 0)
            tier = 0;
    }
}
